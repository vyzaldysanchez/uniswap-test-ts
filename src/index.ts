import Web3 from 'web3';

import UNISWAP_ABI from './abi.json';


const uniswapMainnetAddress = '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D';

async function run() {
	const web3 = new Web3('wss://mainnet.infura.io/ws/v3/4d8ae57d50bc41c497dcf8f801a9c8ba');

	const contract = new web3.eth.Contract(
		UNISWAP_ABI as any,
		uniswapMainnetAddress,
		{ from: 'address-sending-requests' }
	);

	const addresses = [
		'0xdac17f958d2ee523a2206206994597c13d831ec7', // USDT
		'0xc02aaa39b223fe8d0a0e5c4f27ead9083c756cc2' // WETH
	];

	console.log('getAmountsIn', await contract.methods.getAmountsIn('1000000000000000000', addresses).call());
	console.log('getAmountsOut', await contract.methods.getAmountsOut('1000000000000000000', addresses).call());
}

run();
